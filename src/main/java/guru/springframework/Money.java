package guru.springframework;


public class Money implements Expression  {
    protected int amount;
    protected String currency;

    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    protected String currency() {
        return currency;
    }
    public static Money dollar(int amount) {
        return new Money(amount, "usd");
    }
    public static Money franc(int amount) {
        return new Money(amount, "frc");
    }
    public Money times(int multiplier) {
        return new Money(amount * multiplier, this.currency);
    }

@Override
public Money reduce(String to){
        return this;

}


    public boolean equals(Object o) {
        Money dollar = (Money) o;
        return amount == dollar.amount
                && getClass().equals(o.getClass())
                ;
    }

    @Override
    public String toString() {
        return "Money{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }

    public Expression plus(Money money){
        return  new Sum(this,money);
    }
}
